package ru.t1.rleonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.IUserOwnedRepository;
import ru.t1.rleonov.tm.model.AbstractUserOwnedModel;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public int getSize(@NotNull final String userId) {
        return (int) models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

    @Nullable
    @Override
    public M add(
            @Nullable final String userId,
            @NotNull final M model
    ) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        return models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(
            @NotNull final String userId,
            @NotNull final Comparator comparator
    ) {
        @NotNull final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public boolean existsById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public M findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || id == null) return null;
        return models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public M findOneByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        return findAll(userId).get(index);
    }

    @Nullable
    @Override
    public M remove(
            @Nullable final String userId,
            @Nullable final M model
    ) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Nullable
    @Override
    public M removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @NotNull
    @Override
    public M removeByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @NotNull final M model = findOneByIndex(userId, index);
        return remove(model);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<M> result = findAll(userId);
        models.removeAll(result);
    }

}
